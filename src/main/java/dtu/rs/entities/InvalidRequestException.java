package dtu.rs.entities;

public class InvalidRequestException extends Exception
{
    public InvalidRequestException(String message)
    {
        super(message);
    }

}
