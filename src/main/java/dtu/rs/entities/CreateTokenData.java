package dtu.rs.entities;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class CreateTokenData {
    private String cid;
    private int number;

    @JsonbCreator
    public CreateTokenData(@JsonbProperty("cid") String cid,
                           @JsonbProperty("number") int number){
        this.cid = cid;
        this.number = number;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}