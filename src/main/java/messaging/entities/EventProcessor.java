package messaging.entities;

/**
 * Manager able to process event messages
 * @author: Peter
 */
public interface EventProcessor {
    void processMessage(String owner, EVENT_TYPE type, Object content, String extra) throws Exception;
}

