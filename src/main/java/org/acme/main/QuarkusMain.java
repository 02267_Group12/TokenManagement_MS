package org.acme.main;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import manager.TokenManager;
import messaging.entities.EventProcessor;
import messaging.mq.MQListener;

@io.quarkus.runtime.annotations.QuarkusMain
public class QuarkusMain {
    public static void main(String ... args) {
        System.out.println("Running main method");
        System.out.println("Running Quarkus...");
        Quarkus.run(QuarkusApp.class, args);
    }

    public static class QuarkusApp implements QuarkusApplication {
        @Override
        public int run(String... args) throws Exception {
            Runnable startUp = new Runnable() {
                @Override
                public void run() {
                    System.out.println("Run MQServer and start listening...");
                    EventProcessor manager = new TokenManager();
                    MQListener listener = new MQListener("Token management", manager);
                    listener.listen(new String[] {"token.*"});
                }
            };
            Thread newThread = new Thread(startUp);
            newThread.start();
            Quarkus.waitForExit();
            return 0;
        }
    }
}
