package org.acme;

import manager.TokenManager;
import dtu.rs.entities.InvalidRequestException;
import dtu.rs.entities.InvalidUseException;
import dtu.rs.entities.Token;
import dtu.rs.entities.CreateTokenData;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Token Management REST API
 * @author: Jordi, Blanca, Victoria and Peter
 */
@Path("/tokens")
public class TokenManagerResource
{
    private static final String SERVICE_NAME = "Token Manager";
    private TokenManager tokenManager = new TokenManager();

    private boolean isAccountExisting(String cid){
       return true;
    }

    @GET
    @Path("/customer/{cid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokensFromCustomerId(@PathParam("cid") String cid)
    {
        try {
            if(isAccountExisting(cid)) {
                List<Token> test = tokenManager.getCustomerTokens(cid);
                Response build = Response
                        .ok(new GenericEntity<List<Token>>(test) {
                        })
                        .build();
                return build;
            }else{
                return Response
                        .status(404, "Could not find customer with id " + cid)
                        .build();
            }
        } catch (Exception e){
                e.printStackTrace();
            return Response
                    .status(404, "Could not find customer with id " + cid)
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTokens()
    {
        try {
            return Response
                    .ok(new GenericEntity<List<Token>> (tokenManager.getAllTokens()){})
                    .build();
        } catch (Exception e){
            e.printStackTrace();
            return Response
                    .status(404, "Failed to give all tokens")
                    .build();
        }
    }

    @GET
    @Path("/{tid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToken(@PathParam("tid") String tid)
    {
        if (tokenManager.isValidToken(tid)){
            return Response.ok(tokenManager.getToken(tid)).build();
        } else {
            return Response.status(404, "Token not found").build();
        }
    }

    @GET
    @Path("/valid/{tid}")
    public Response tokenIsValid(@PathParam("tid") String tid)
    {
        if (tokenManager.isValidToken(tid))
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(404, "Token not valid").build();
        }
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateTokensForCustomer(CreateTokenData tokenData)
    {
        try{
            if(isAccountExisting(tokenData.getCid())) {
                ArrayList<Token> tokens = tokenManager.generateTokensForCustomer(tokenData.getCid(), tokenData.getNumber());
                return Response
                        .ok(new GenericEntity<List<Token>>(tokens) {
                        })
                        .build();
            }else{
                return Response
                        .status(404, "Customer cannot request tokens.")
                        .build();
            }
        } catch (InvalidRequestException e)
        {
            return Response
                    .status(404, "Customer cannot request tokens.")
                    .build();
        }
    }

    @DELETE
    @Path("/{tid}")
    public Response invalidateToken(@PathParam("tid") String tid)
    {
        try {
            tokenManager.useToken(tid);
            return Response.ok().build();
        } catch (InvalidUseException e) {
            return Response
                    .status(404, "Could not invalidate token.")
                    .build();
        }
    }

    @DELETE
    public Response clean()
    {
        tokenManager.clean();
        return Response.ok().build();
    }


}
