package manager;

import dtu.rs.entities.*;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.entities.EventProcessor;
import messaging.mq.MQSender;

import java.util.*;

/**
 * Token Manager
 * @author: Jordi, Blanca and Victoria
 */
public class TokenManager implements EventProcessor {
    private static HashMap<String, Set<Token>> customerTokens = new HashMap<String, Set<Token>>();
    private static HashMap<String, Token> pullTokens = new HashMap<>();
    private static HashMap<String, String> mapTokenToCustomer = new HashMap<>();

    private static final String SERVICE_NAME = "Token Manager";

    private String generateID(){
        return UUID.randomUUID().toString();
    }

    private Token newToken(){
        Token token = new Token();
        token.setId(generateID());
        token.setValid(true);
        return token;
    }

    public String tokenToCustomer(String tid)
    {
        return mapTokenToCustomer.get(tid);
    }

    public Token generateToken()
    {
        Token token = newToken();
        while(pullTokens.containsKey(token.getId())){
            token = newToken();
        }
        pullTokens.put(token.getId(), token);
        return token;
    }

    public List<Token> getAllTokens(){
        return  new ArrayList<Token>(pullTokens.values());
    }

    // ProcessTokenRequest
    public ArrayList<Token> generateTokensForCustomer(String cid, int numberTokens) throws InvalidRequestException
    {
        if ( numberTokens < 1 || 5 < numberTokens)
        {
            throw new InvalidRequestException("Cannot demand more than 5 or less than 1 token");
        }
        if (!customerTokens.containsKey(cid))
        {
            customerTokens.put(cid, new HashSet<Token>());
        }
        Set<Token> tokens = customerTokens.get(cid);

        // If the user has more than 1 unused token and he requests again a set of tokens, his request will be denied
        if (tokens.size() > 1 || tokens.size() + numberTokens > 6) {
            throw new InvalidRequestException("Cannot demand more than 5 or less than 1 token");
        }
        ArrayList<Token> newTokens = new ArrayList<Token>();
        Token token;
        for (int i = 0; i < numberTokens; i++) {
            token = generateToken();
            tokens.add(token);
            pullTokens.put(token.getId(), token);
            mapTokenToCustomer.put(token.getId(), cid);
            newTokens.add(token);
        }
        return newTokens;
    }

    public void useToken(String tid) throws InvalidUseException {
        String cid = mapTokenToCustomer.get(tid);
        if (customerHasToken(cid, tid))
        {
            Token token =  pullTokens.get(tid);
            customerTokens.get(cid).remove(token);
            pullTokens.get(token.getId()).invalidate();
            mapTokenToCustomer.remove(tid);
        }
        else
        {
            throw new InvalidUseException("User possess no such token.");
        }
    }
    
    public boolean customerHasToken(String cid, String tid)
    {
        Token token = pullTokens.get(tid);
        try{
            return getCustomerTokens(cid).contains(token);
        } catch (CustomerNotFoundException e) {
            return false;
        }
    }

    public Boolean isValidToken(String tid){
        try {
            return pullTokens.get(tid).isValid();
        } catch (NullPointerException e){
            return false;
        }
    }

    public ArrayList<Token> getCustomerTokens(String cid) throws CustomerNotFoundException
    {
        if (customerTokens.containsKey(cid))
        {
            return new ArrayList(customerTokens.get(cid));
        } else
        {
            throw new CustomerNotFoundException("Customer with id " + cid + " has no tokens." );
        }
    }

    public void removeTokensForUser(String cid)
    {
        customerTokens.remove(cid);
    }

    
    public Token getToken(String tid)
    {
        return pullTokens.get(tid);
    }


    public int howManyTokens(String cid)
    {
        try{
            return customerTokens.get(cid).size();
        } catch (NullPointerException e)
        {
            return 0;
        }
    }

    public void clean(){
        customerTokens = new HashMap<String, Set<Token>>();
        pullTokens = new HashMap<>();
        mapTokenToCustomer = new HashMap<>();
    }

    @Override
    public void processMessage(String owner, EVENT_TYPE type, Object content, String extra) throws Exception {
        if(type == EVENT_TYPE.DELETE_TOKENS) {
            removeTokensForUser((String) content);
            System.out.println("Tokens for user "+content+" has been deleted.");
        }else if(type == EVENT_TYPE.TOKEN_USE){
            useToken((String) content);
        }else if(type == EVENT_TYPE.TOKEN_GET_USER_ID){
            String user_id = tokenToCustomer((String) content);
            Event event = new Event(EVENT_TYPE.RESPONSE_TOKEN_GET_USER_ID, "Token Manager", user_id, String.class.getName());
            event.setExtra(extra);
            new MQSender().sendEvent(event, "response.get_cid_token");
        }
    }
}
