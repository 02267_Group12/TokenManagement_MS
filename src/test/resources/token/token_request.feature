Feature: Request tokens

  @CucumberTest.TokenManager.Request
  Scenario: Customer "cid1" with valid tokens fails request of 3 tokens
    Given a customer with id "cid1" has 2 valid tokens
    When the customer requests 3 tokens
    Then the request is denied

  @CucumberTest.TokenManager.Request
  Scenario: Customer "cid1" with 1 requests 1 token
    Given a customer with id "cid1" has 1 valid tokens
    When the customer requests 3 tokens
    Then the customer has 4 valid tokens

  @CucumberTest.TokenManager.Request
  Scenario: Customer "cid1" requests too many tokens
    Given a customer with id "cid1" has 1 valid tokens
    When the customer requests 7 tokens
    Then the request is denied

  @CucumberTest.TokenManager.Request
  Scenario:  Customer "cid1" requests tokens for the first time
    Given a new customer with cid "cid1"
    When the customer requests 4 tokens
    Then the customer has 4 valid tokens
