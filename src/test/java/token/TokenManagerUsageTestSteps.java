package token;

import dtu.rs.entities.InvalidRequestException;
import dtu.rs.entities.InvalidUseException;
import dtu.rs.entities.Token;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.List;

import io.cucumber.junit.platform.engine.Cucumber;
import manager.TokenManager;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

@Cucumber
public class TokenManagerUsageTestSteps {

    TokenManager tm = new TokenManager();
    String cid;
    List<Token> customerTokens = new ArrayList<Token>();
    //static HashMap<String, Set<Token>> tokens = new HashMap<String, Set<Token>>();

    @Given("a customer with {int} tokens")
    public void aCustomerWithTokens(int numberTokens) throws InvalidRequestException {
        this.cid = "cid1";
        customerTokens = tm.generateTokensForCustomer(this.cid, numberTokens);
    }

    @When("the customer uses {int} valid tokens")
    public void theCustomerUsesValidTokens(int numberTokens) throws InvalidUseException {
        try{
            for (int i = 0; i < numberTokens; i++) {
                tm.useToken(customerTokens.get(i).getId());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Then("after usage the customer has {int} tokens")
    public void theCustomerHasValidTokens(int howMany) {
        Assertions.assertEquals(howMany, tm.howManyTokens(cid));
    }



    @When("the customer uses {int} invalid tokens")
    public void theCustomerUsesInvalidTokens(int numberTokens){
        int num_invalid = 0;
        boolean fails = false;
        for (int i = 0; i < numberTokens; i++) {
            try {
                tm.useToken("foobarbaz");
            } catch (InvalidUseException e){
                num_invalid += 1;
                continue;
            }
            fails = true;
            break;
        }
        Assert.assertFalse(fails);
    }

    @After
    public void clearTokensAndUser()
    {
        tm.removeTokensForUser(cid);
        this.cid = null;
    }

}
