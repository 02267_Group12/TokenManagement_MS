package token;

import dtu.rs.entities.InvalidRequestException;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.TokenManager;
import org.junit.jupiter.api.Assertions;


public class TokenManagerRequestTestSteps
{

    TokenManager tokenManager = new TokenManager();
    String cid;
    boolean success = true;

    @Given("a customer with id {string} has {int} valid tokens")
    public void aCustomerWithIdHasValidTokens(String cid, Integer howMany) throws InvalidRequestException {
        this.cid = cid;
        tokenManager.generateTokensForCustomer(cid, howMany);
    }

    @When("the customer requests {int} tokens")
    public void theCustomerRequestsIntTokens(int howMany) {
        try{
            tokenManager.generateTokensForCustomer(cid, howMany);
            this.success = true;
        }
        catch (InvalidRequestException e)
        {
            this.success = false;
        }
    }

    @Then("the request is denied")
    public void theRequestIsDenied() {
        Assertions.assertFalse(success);
    }

    @Then("the customer has {int} valid tokens")
    public void theCustomerHasIntTokens(int howMany) {
        Assertions.assertEquals(howMany, tokenManager.howManyTokens(cid));
    }

    @After
    public void clearTokensAndUser()
    {
        tokenManager.removeTokensForUser(cid);
        this.cid = null;
    }

    @Given("a new customer with cid {string}")
    public void aNewCustomerWithCid(String cid) {
        this.cid = cid;
    }
}
