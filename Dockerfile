FROM openjdk:11

ARG JAR_FILE=TokenManagement_MS-1.0.0-SNAPSHOT-runner.jar
ENV JAR_FILE=$JAR_FILE

WORKDIR /usr/src/
COPY target/lib /usr/src/lib
COPY target/$JAR_FILE /usr/src

CMD java -jar $JAR_FILE